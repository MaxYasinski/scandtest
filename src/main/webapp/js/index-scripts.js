$(document).ready(function () {
    handleUserIsLoggedIn();

    $(".logInPanel").submit(validateForm);

    $(".logoutButton").click(ajaxLogout);

});

function validateForm() {
    var loginInput = $("#login");
    var passwordInput = $("#password");
    var errorMessageBox = $("#errorMessageBox");
    if (loginInput.val() == "") {
        errorMessageBox.text("Please, enter your login");
        loginInput.focus();
        return false;
    }
    else if (passwordInput.val() == "") {
        errorMessageBox.text("Please, enter your password");
        passwordInput.focus();
        return false;
    } else {
        return doLogin();
    }
}

function doLogin() {
    ajaxLogin();
    return false;
}

function ajaxLogin() {
    var loginInput = $("#login");
    var passwordInput = $("#password");
    var languageSelect = $("#languageSelect");
    var rememberMe = $('#rememberMe');
    $.post(
        "/login/",
        {
            login: loginInput.val(),
            password: passwordInput.val(),
            language: languageSelect.val(),
            rememberMe: rememberMe.attr("checked")
        },
        handleLogin
    );
}

function handleLogin(data) {
    if (data === "") {
        $("#errorMessageBox").text("Bad credentials");
        $("#password").val("");
        $("#rememberMe").attr("checked", false);
        showLoginPanel();
    } else {
        showMainPanel(data);
    }
}

function ajaxLogout() {
    $.post(
        "/logout/",
        {},
        showLoginPanel
    );
}

function showLoginPanel() {
    $(".logInPanel").show();
    $(".mainPanel").hide();
}

function showMainPanel(data) {
    $(".logInPanel").hide();
    $(".mainPanel").show();
    $(".usernameData").text((data.firstName).concat(" ").concat(data.lastName).concat("!"));
    $("#flag").attr("src", ("/css/images/").concat(data.language).concat("_big.png"));
    loadProperties(data.language);
}

function loadProperties(language) {
    $.i18n.properties({
        name: 'messages',
        path: '/messages/',
        mode: 'both',
        language: language,
        callback: insertTranslated
    });
}

function insertTranslated() {
    $(".welcomePanel .staticText").text($.i18n.prop("msg_welcome"));
    $(".languagePanel .staticText").text($.i18n.prop("msg_your_language"));
    $(".languagePanel .languageData").text($.i18n.prop("msg_language_value"));
    $(".logoutButton").text($.i18n.prop("msg_logout"));
}

