<%@ page import="com.yasinskim.model.User" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<% User user = (User) request.getAttribute("user"); %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script src="/js/lib/jquery.i18n.properties-min-1.0.9.js"></script>
    <script src="/js/index-scripts.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/styleIndex.css"/>
    <script>
        function handleUserIsLoggedIn() {
            var user = "";
            <c:if test="${user!=null}">
            user = {
                login: '<%=user.getLogin()%>',
                password: '<%=user.getPassword()%>',
                firstName: '<%=user.getFirstName()%>',
                lastName: '<%=user.getLastName()%>',
                language: '<%=user.getLanguage()%>'
            };
            </c:if>
            if (user === "") {
                showLoginPanel();
            } else {
                showMainPanel(user);
            }
        }
    </script>
    <title>Scand Test</title>
</head>
<body>
<form name="logInPanel" class="logInPanel" action="javascript:void(0);">
    <table id="loginFormTable">
        <tr>
            <td colspan="2" class="loginFormMessage">Hi! Fill the login form</td>
        </tr>
        <tr>
            <td id="errorMessageBox" colspan="2"></td>
        </tr>
        <tr>
            <td>Login</td>
            <td><input type="text" name="login" id="login"/></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id="password"/></td>
        </tr>
        <tr>
            <td>Language</td>
            <td><select name="language" id="languageSelect">
                <option value="EN" id="optionDef" selected="selected"></option>
                <option value="EN" id="optionEN">English</option>
                <option value="RU" id="optionRU">Русский</option>
                <option value="FR" id="optionFR">Francaise</option>
                <option value="DE" id="optionDE">Deutsch</option>
            </select></td>
        </tr>
        <tr>
            <td colspan="2">Remember me<label><input type="checkbox" name="rememberMe" id="rememberMe"/></label></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Log In"/></td>
        </tr>
    </table>
</form>

<div class="mainPanel">
    <table class="mainInfo">
        <tr class="welcomePanel">
            <td colspan="2">
                <label class="staticText"></label>
                <label class="usernameData"></label>
            </td>
        </tr>
        <tr class="languagePanel">
            <td>
                <label class="staticText"></label><br/>
                <label class="languageData"></label>
            </td>
            <td>
                <img id="flag" src=""/>
            </td>
        </tr>
        <tr class="logoutPanel">
            <td colspan="2">
                <label class="logoutButton"></label>
            </td>
        </tr>
    </table>
</div>
</body>
</html>