package com.yasinskim.dao;

import com.yasinskim.model.User;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

public interface UserDAO {

    public void save(User user);

    public void update(User user);

    public User get(String userId);

    public User getUserByLogin(String username);

    public void delete(String username);

    public String getPassword(int userId);

}
