package com.yasinskim.dao.impl;

import com.yasinskim.dao.UserDAO;
import com.yasinskim.model.User;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(User user) {
        getSession().save(user);
    }

    public void update(User user) {
        getSession().update(user);
    }

    public void delete(String login) {
        User user = getUserByLogin(login);
        if (user != null) {
            getSession().delete(user);
        }
    }

    public User get(String userId) {
        return (User) getSession().createCriteria(User.class)
                .add(Restrictions.eq("id", userId))
                .uniqueResult();
    }

    public User getUserByLogin(String login) {
        return (User) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login))
                .setFetchMode("authorities", FetchMode.JOIN)
                .uniqueResult();
    }

    public String getPassword(int userId) {
        return (String) getSession().createCriteria(User.class)
                .setProjection(Projections.property("password"))
                .add(Restrictions.eq("id", userId))
                .uniqueResult();
    }

}
