package com.yasinskim.service;

import com.yasinskim.dao.UserDAO;
import com.yasinskim.model.User;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

public class UserService {

    private UserDAO userDAO;

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void save(User user) {
        userDAO.save(user);
    }

    public void update(User user) {
        userDAO.update(user);
    }

    public User getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }
}
