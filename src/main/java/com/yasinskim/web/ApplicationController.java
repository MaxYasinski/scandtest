package com.yasinskim.web;

import com.yasinskim.model.User;
import com.yasinskim.service.UserService;
import org.springframework.aop.framework.AopProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

@Controller
public class ApplicationController {
    @Autowired
    private UserService userService;
    @Autowired
    private SessionContext sessionContext;
    @Autowired
    private ShaPasswordEncoder passwordEncoder;
    private static final String USER_ATTRIBUTE_NAME = "user";
    private static final String DEFAULT_LANGUAGE = "EN";
    private static final String LOGIN_COOKIE_NAME = "login";
    private static final String PASSWORD_COOKIE_NAME = "password";
    private static final String LANGUAGE_COOKIE_NAME = "language";
    private static final String SALT = "ScandSalt";
    private static final int COOKIE_EXPIRES_IN = 604800; // 1 week (in seconds)


    @RequestMapping(value = "/ScandTest/", method = RequestMethod.GET)
    public String enter(HttpServletRequest request, HttpServletResponse response) {
        User user = sessionContext.getUser();
        if (user == null) {
            user = tryToLogin(request, response);
        }
        request.setAttribute(USER_ATTRIBUTE_NAME, user);
        return "/index";
    }

//  In case of user's absence in the session this method checks if "auth-cookies" exist.
//  If exist - returns logged-in user
//  Else  returns null.
    private User tryToLogin(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }
        String login = null;
        String password = null;
        String language = DEFAULT_LANGUAGE;
        for (Cookie cookie : cookies) {
            if (LOGIN_COOKIE_NAME.equals(cookie.getName())) {
                login = cookie.getValue();
            } else if (PASSWORD_COOKIE_NAME.equals(cookie.getName())) {
                password = cookie.getValue();
            } else if (LANGUAGE_COOKIE_NAME.equals(cookie.getName())) {
                language = cookie.getValue();
            }
        }
        if (login != null && password != null) {
            return doLogin(login, password, language, false, request, response);
        }
        return null;
    }

    @RequestMapping(value = "/login/", method = RequestMethod.POST)
    public
    @ResponseBody //Paste return value to the web response body
    User logIn(@RequestParam String login, @RequestParam String password, @RequestParam String language,
               @RequestParam boolean rememberMe, HttpServletRequest request, HttpServletResponse response) {
        return doLogin(login, encode(password), language, rememberMe, request, response);
    }

    private User doLogin(String login, String encryptedPassword, String language, boolean rememberMe,
                         HttpServletRequest request, HttpServletResponse response) {
        User user = userService.getUserByLogin(login);
        if (user == null || !encryptedPassword.equals(user.getPassword())) {
            doLogout(request, response);
            return null;
        } else {
            if (rememberMe) {
                addCookie(LOGIN_COOKIE_NAME, login, response);
                addCookie(PASSWORD_COOKIE_NAME, encryptedPassword, response);
                addCookie(LANGUAGE_COOKIE_NAME, language, response);
            }
            if (!language.equals(user.getLanguage())) {
                user.setLanguage(language);
                userService.update(user);
            }
            sessionContext.setUser(user);
            return user;
        }
    }

    @RequestMapping(value = "/logout/", method = RequestMethod.POST)
    private void doLogout(HttpServletRequest request, HttpServletResponse response) {
        sessionContext.setUser(null);
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            killCookie(cookie.getName(), response);
        }
    }

    private void addCookie(String cookieName, String cookieValue, HttpServletResponse response) {
        Cookie cookie = new Cookie(cookieName, cookieValue);
        cookie.setMaxAge(COOKIE_EXPIRES_IN);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    private void killCookie(String cookieName, HttpServletResponse response) {
        response.setContentType("text/html");
        Cookie killMyCookie = new Cookie(cookieName, null);
        killMyCookie.setMaxAge(0);
        killMyCookie.setPath("/");
        response.addCookie(killMyCookie);
    }

    private String encode(String password) {
        return passwordEncoder.encodePassword(password, SALT);
    }

}

