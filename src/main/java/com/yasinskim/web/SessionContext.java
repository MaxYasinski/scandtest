package com.yasinskim.web;

import com.yasinskim.model.User;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

//Class for holding user info only with his session (scope="session")
public class SessionContext {

    private User user;

    public SessionContext() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
