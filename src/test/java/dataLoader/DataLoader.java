package dataLoader;

import com.yasinskim.model.User;
import com.yasinskim.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * @author <a href="yasinskim@gmail.com">Max Yasinski</a>
 */

// Is useful for data uploading to DB.
// Placed here (test dir) just for separation from main logic
public class DataLoader {
    private UserService userService;
    private ShaPasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        DataLoader loader = new DataLoader();
        loader.init();
        loader.upload();
    }

    private void init() {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("spring-configuration/common.xml");
        userService = (UserService) appContext.getBean("userService");
        passwordEncoder = (ShaPasswordEncoder) appContext.getBean("passwordEncoder");
    }

    private void upload() {
        User user1 = new User("jan", encode("janpass"), "Ivan", "Ivanov", "RU");
        User user2 = new User("hellmut", encode("kolt"), "Helmut", "Kohl", "DE");
        User user3 = new User("ev", encode("monpasse"), "Eva", "Paris", "FR");
        User user4 = new User("admin", encode("admim"), "Admin", "Adminov");
        userService.save(user1);
        userService.save(user2);
        userService.save(user3);
        userService.save(user4);
    }

    private String encode(String password) {
        return passwordEncoder.encodePassword(password, "ScandSalt");
    }
}
